<?php
namespace Northern\GoogleRecaptcha\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class WishlistIndexSendObserver implements ObserverInterface {
    /**
     * @var \Northern\GoogleRecaptcha\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    protected $actionFlag;

    /**
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlManager;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * WishlistIndexSendObserver constructor.
     *
     * @param \Magento\Framework\Message\ManagerInterface       $messageManager
     * @param \Magento\Framework\App\ActionFlag                 $actionFlag
     * @param \Magento\Framework\UrlInterface                   $urlManager
     * @param \Northern\GoogleRecaptcha\Helper\Data             $helper
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\UrlInterface $urlManager,
        \Northern\GoogleRecaptcha\Helper\Data $helper,
        \Magento\Framework\App\Response\RedirectInterface $redirect
    ) {
        $this->messageManager = $messageManager;
        $this->urlManager     = $urlManager;
        $this->helper         = $helper;
        $this->actionFlag     = $actionFlag;
        $this->redirect       = $redirect;
    }

    /**
     * Check captcha on user login page
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @throws NoSuchEntityException
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $formId = 'wishlist_sharing';
        if ($this->helper->isCaptcha($formId)) {
            /**
             * @var \Magento\Framework\App\Action\Action $controller
             */
            $controller = $observer->getControllerAction();
            $data       = $controller->getRequest()->getPost();
            $gData      = isset($data['g-recaptcha-response']) ? $data['g-recaptcha-response'] : null;
            if (!$gData || !$this->helper->verifyResponse($gData)) {
                $this->messageManager->addErrorMessage(__('Incorrect Google reCAPTCHA'));
                $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $controller->getResponse()->setRedirect($this->redirect->getRefererUrl());
            }
        }

        return $this;
    }
}
