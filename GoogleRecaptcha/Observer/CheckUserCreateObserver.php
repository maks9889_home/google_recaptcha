<?php
namespace Northern\GoogleRecaptcha\Observer;

use Magento\Framework\Event\ObserverInterface;

class CheckUserCreateObserver implements ObserverInterface {
    /**
     * @var \Magento\Captcha\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    protected $actionFlag;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $session;

    /**
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlManager;

    /**
     * @var CaptchaStringResolver
     */
    protected $captchaStringResolver;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @param \Magento\Captcha\Helper\Data                       $helper
     * @param \Magento\Framework\App\ActionFlag                  $actionFlag
     * @param \Magento\Framework\Message\ManagerInterface        $messageManager
     * @param \Magento\Framework\Session\SessionManagerInterface $session
     * @param \Magento\Framework\UrlInterface                    $urlManager
     * @param \Magento\Framework\App\Response\RedirectInterface  $redirect
     * @param CaptchaStringResolver                              $captchaStringResolver
     */
    public function __construct(
        \Northern\GoogleRecaptcha\Helper\Data $helper,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Session\SessionManagerInterface $session,
        \Magento\Framework\UrlInterface $urlManager,
        \Magento\Framework\App\Response\RedirectInterface $redirect
    ) {
        $this->helper         = $helper;
        $this->actionFlag     = $actionFlag;
        $this->messageManager = $messageManager;
        $this->session        = $session;
        $this->urlManager     = $urlManager;
        $this->redirect       = $redirect;
    }

    /**
     * Check Captcha On User Login Page
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $formId = 'user_create';
        if ($this->helper->isCaptcha($formId)) {
            $controller = $observer->getControllerAction();
            $data       = $controller->getRequest()->getPost();
            $gData      = isset($data['g-recaptcha-response']) ? $data['g-recaptcha-response'] : null;
            if (!$gData || !$this->helper->verifyResponse($gData)) {
                $this->messageManager->addErrorMessage(__('Incorrect Google reCAPTCHA'));
                $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $this->session->setCustomerFormData($controller->getRequest()->getPostValue());
                $url = $this->urlManager->getUrl('*/*/create', ['_nosecret' => true]);
                $controller->getResponse()->setRedirect($this->redirect->error($url));
            }
        }

        return $this;
    }
}
