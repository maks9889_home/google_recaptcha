<?php
namespace Northern\GoogleRecaptcha\Observer;

use Magento\Customer\Model\AuthenticationInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CheckUserEditObserver implements ObserverInterface {
    /**
     * Form ID
     */
    const FORM_ID = 'user_edit';

    /**
     * @var \Magento\Captcha\Helper\Data|\Northern\GoogleRecaptcha\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    protected $actionFlag;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @var CaptchaStringResolver
     */
    protected $captchaStringResolver;

    /**
     * Authentication
     *
     * @var AuthenticationInterface
     */
    protected $authentication;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * CheckUserEditObserver constructor.
     *
     * @param \Northern\GoogleRecaptcha\Helper\Data              $helper
     * @param \Magento\Framework\App\ActionFlag                  $actionFlag
     * @param \Magento\Framework\Message\ManagerInterface        $messageManager
     * @param \Magento\Framework\App\Response\RedirectInterface  $redirect
     * @param \Magento\Customer\Model\AuthenticationInterface    $authentication
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Northern\GoogleRecaptcha\Helper\Data $helper,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        AuthenticationInterface $authentication,
        Session $customerSession,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->helper          = $helper;
        $this->actionFlag      = $actionFlag;
        $this->messageManager  = $messageManager;
        $this->redirect        = $redirect;
        $this->authentication  = $authentication;
        $this->customerSession = $customerSession;
        $this->scopeConfig     = $scopeConfig;
    }

    /**
     * Check Captcha On Forgot Password Page
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        if ($this->helper->isCaptcha(self::FORM_ID)) {
            $controller = $observer->getControllerAction();
            $data       = $controller->getRequest()->getPost();
            $gData      = isset($data['g-recaptcha-response']) ? $data['g-recaptcha-response'] : null;
            if (!$gData || !$this->helper->verifyResponse($gData)) {
                $customerId = $this->customerSession->getCustomerId();
                $this->authentication->processAuthenticationFailure($customerId);
                if ($this->authentication->isLocked($customerId)) {
                    $this->customerSession->logout();
                    $this->customerSession->start();
                    $message = __(
                        'The account is locked. Please wait and try again or contact %1.',
                        $this->scopeConfig->getValue('contact/email/recipient_email')
                    );
                    $this->messageManager->addErrorMessage($message);
                }
                $this->messageManager->addErrorMessage(__('Incorrect Google reCAPTCHA'));
                $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $this->redirect->redirect($controller->getResponse(), '*/*/edit');
            }
        }

        return $this;
    }
}
