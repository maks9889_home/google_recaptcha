<?php
namespace Northern\GoogleRecaptcha\Observer;

use Magento\Framework\Event\ObserverInterface;

class CheckGuestCheckoutObserver implements ObserverInterface {
    /**
     * @var \Northern\GoogleRecaptcha\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    protected $_actionFlag;

    /**
     * @var CaptchaStringResolver
     */
    protected $captchaStringResolver;

    /**
     * @var \Magento\Checkout\Model\Type\Onepage
     */
    protected $_typeOnepage;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * CheckGuestCheckoutObserver constructor.
     *
     * @param \Northern\GoogleRecaptcha\Helper\Data                    $helper
     * @param \Magento\Framework\App\ActionFlag                        $actionFlag
     * @param \Northern\GoogleRecaptcha\Observer\CaptchaStringResolver $captchaStringResolver
     * @param \Magento\Checkout\Model\Type\Onepage                     $typeOnepage
     * @param \Magento\Framework\Json\Helper\Data                      $jsonHelper
     */
    public function __construct(
        \Northern\GoogleRecaptcha\Helper\Data $helper,
        \Magento\Framework\App\ActionFlag $actionFlag,
        CaptchaStringResolver $captchaStringResolver,
        \Magento\Checkout\Model\Type\Onepage $typeOnepage,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->_helper               = $helper;
        $this->_actionFlag           = $actionFlag;
        $this->captchaStringResolver = $captchaStringResolver;
        $this->_typeOnepage          = $typeOnepage;
        $this->jsonHelper            = $jsonHelper;
    }

    /**
     * Check Captcha On Checkout as Guest Page
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $formId         = 'guest_checkout';
        $captchaModel   = $this->_helper->getCaptcha($formId);
        $checkoutMethod = $this->_typeOnepage->getQuote()->getCheckoutMethod();
        if ($checkoutMethod == \Magento\Checkout\Model\Type\Onepage::METHOD_GUEST) {
            if ($captchaModel->isRequired()) {
                $controller = $observer->getControllerAction();
                if (!$captchaModel->isCorrect($this->captchaStringResolver->resolve($controller->getRequest(), $formId))
                ) {
                    $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                    $result = ['error' => 1, 'message' => __('Incorrect CAPTCHA')];
                    $controller->getResponse()->representJson($this->jsonHelper->jsonEncode($result));
                }
            }
        }

        return $this;
    }
}
