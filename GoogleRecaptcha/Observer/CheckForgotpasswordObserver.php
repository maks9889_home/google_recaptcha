<?php
namespace Northern\GoogleRecaptcha\Observer;

use Magento\Framework\Event\ObserverInterface;

class CheckForgotpasswordObserver implements ObserverInterface {
    /**
     * @var \Northern\GoogleRecaptcha\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $session;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * Customer data
     *
     * @var \Magento\Customer\Model\Url
     */
    protected $customerUrl;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    protected $actionFlag;

    /**
     * CheckForgotpasswordObserver constructor.
     *
     * @param \Magento\Framework\Message\ManagerInterface        $messageManager
     * @param \Magento\Framework\App\ActionFlag                  $actionFlag
     * @param \Magento\Framework\Session\SessionManagerInterface $customerSession
     * @param \Magento\Customer\Model\Url                        $customerUrl
     * @param \Magento\Framework\App\Response\RedirectInterface  $redirect
     * @param \Northern\GoogleRecaptcha\Helper\Data              $helper
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\Session\SessionManagerInterface $customerSession,
        \Magento\Customer\Model\Url $customerUrl,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Northern\GoogleRecaptcha\Helper\Data $helper
    ) {
        $this->messageManager = $messageManager;
        $this->session        = $customerSession;
        $this->customerUrl    = $customerUrl;
        $this->helper         = $helper;
        $this->redirect       = $redirect;
        $this->actionFlag     = $actionFlag;
    }

    /**
     * Check Captcha On Forgot Password Page
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $formId = 'user_forgotpassword';
        if ($this->helper->isCaptcha($formId)) {
            $controller = $observer->getControllerAction();
            $data       = $controller->getRequest()->getPost();
            $gData      = isset($data['g-recaptcha-response']) ? $data['g-recaptcha-response'] : null;
            if (!$gData || !$this->helper->verifyResponse($gData)) {
                $this->messageManager->addErrorMessage(__('Incorrect Google reCAPTCHA'));
                $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $this->redirect->redirect($controller->getResponse(), '*/*/forgotpassword');
            }
        }

        return $this;
    }
}
