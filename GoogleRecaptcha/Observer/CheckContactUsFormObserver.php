<?php
namespace Northern\GoogleRecaptcha\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;

class CheckContactUsFormObserver implements ObserverInterface {
    /**
     * @var \Northern\GoogleRecaptcha\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    protected $actionFlag;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * CheckContactUsFormObserver constructor.
     *
     * @param \Northern\GoogleRecaptcha\Helper\Data             $helper
     * @param \Magento\Framework\App\ActionFlag                 $actionFlag
     * @param \Magento\Framework\Message\ManagerInterface       $messageManager
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     */
    public function __construct(
        \Northern\GoogleRecaptcha\Helper\Data $helper,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        DataPersistorInterface $dataPersistor
    ) {
        $this->helper         = $helper;
        $this->actionFlag     = $actionFlag;
        $this->messageManager = $messageManager;
        $this->redirect       = $redirect;
        $this->dataPersistor  = $dataPersistor;
    }

    /**
     * Check CAPTCHA on Contact Us page
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $formId = 'contact_us';
        if ($this->helper->isCaptcha($formId)) {
            $controller = $observer->getControllerAction();
            $data       = $controller->getRequest()->getPost();
            $gData      = isset($data['g-recaptcha-response']) ? $data['g-recaptcha-response'] : null;
            if (!$gData || !$this->helper->verifyResponse($gData)) {
                $this->messageManager->addErrorMessage(__('Incorrect Google reCAPTCHA'));
                $this->dataPersistor->set($formId, $controller->getRequest()->getPostValue());
                $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $this->redirect->redirect($controller->getResponse(), 'contact/index/index');
            }
        }
    }
}
