<?php
namespace Northern\GoogleRecaptcha\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CheckUserLoginObserver implements ObserverInterface {
    /**
     * @var \Northern\GoogleRecaptcha\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $session;

    /**
     * Customer data
     *
     * @var \Magento\Customer\Model\Url
     */
    protected $customerUrl;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    protected $actionFlag;

    /**
     * CheckUserLoginObserver constructor.
     *
     * @param \Magento\Framework\Message\ManagerInterface        $messageManager
     * @param \Magento\Framework\App\ActionFlag                  $actionFlag
     * @param \Magento\Framework\Session\SessionManagerInterface $customerSession
     * @param \Magento\Customer\Model\Url                        $customerUrl
     * @param \Northern\GoogleRecaptcha\Helper\Data              $helper
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\Session\SessionManagerInterface $customerSession,
        \Magento\Customer\Model\Url $customerUrl,
        \Northern\GoogleRecaptcha\Helper\Data $helper
    ) {
        $this->messageManager = $messageManager;
        $this->session        = $customerSession;
        $this->customerUrl    = $customerUrl;
        $this->helper         = $helper;
        $this->actionFlag     = $actionFlag;
    }

    /**
     * Check captcha on user login page
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @throws NoSuchEntityException
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $formId = 'user_login';
        if ($this->helper->isCaptcha($formId)) {
            $controller  = $observer->getControllerAction();
            $data        = $controller->getRequest()->getPost();
            $gData       = isset($data['g-recaptcha-response']) ? $data['g-recaptcha-response'] : null;
            $loginParams = $controller->getRequest()->getPost('login');
            $login       = isset($loginParams['username']) ? $loginParams['username'] : null;
            if (!$gData || !$this->helper->verifyResponse($gData)) {
                $this->messageManager->addErrorMessage(__('Incorrect Google reCAPTCHA'));
                $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $this->session->setUsername($login);
                $beforeUrl = $this->session->getBeforeAuthUrl();
                $url       = $beforeUrl ? $beforeUrl : $this->customerUrl->getLoginUrl();
                $controller->getResponse()->setRedirect($url);
            }
        }

        return $this;
    }
}
