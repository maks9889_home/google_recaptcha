<?php
namespace Northern\GoogleRecaptcha\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LogLevel;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {
    const XML_PATH_CUSTOMER_CREATE_ACCOUNT_ENABLE_RECAPTCHA     = 'customer/gcaptcha/enable';
    const XML_PATH_CUSTOMER_CREATE_ACCOUNT_RECAPTCHA_SITE_KEY   = 'customer/gcaptcha/recaptcha_site_key';
    const XML_PATH_CUSTOMER_CREATE_ACCOUNT_RECAPTCHA_SECRET_KEY = 'customer/gcaptcha/recaptcha_secret_key';
    const XML_PATH_CUSTOMER_CREATE_ACCOUNT_RECAPTCHA_FORMS_KEY  = 'customer/gcaptcha/forms';
    const URL_RECAPTCHA_API_SITEVERIFY                          = 'https://www.google.com/recaptcha/api/siteverify';

    protected $encryptor;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor
    ) {
        $this->encryptor = $encryptor;
        parent::__construct($context);
    }

    /**
     * @param null|int|\Mage_Core_Model_Store $store
     *
     * @return bool
     */
    public function getUseRecaptcha($store = null) {
        return $this->scopeConfig->getValue(self::XML_PATH_CUSTOMER_CREATE_ACCOUNT_ENABLE_RECAPTCHA, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $store);
    }

    /**
     * @param null|int|\Mage_Core_Model_Store $store
     *
     * @return mixed
     */
    public function getRecaptchaSiteKey($store = null) {
        return $this->encryptor->decrypt($this->scopeConfig->getValue(self::XML_PATH_CUSTOMER_CREATE_ACCOUNT_RECAPTCHA_SITE_KEY, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $store));
    }

    /**
     * @param null|int|\Mage_Core_Model_Store $store
     *
     * @return mixed
     */
    public function getRecaptchaSecretKey($store = null) {
        return $this->encryptor->decrypt($this->scopeConfig->getValue(self::XML_PATH_CUSTOMER_CREATE_ACCOUNT_RECAPTCHA_SECRET_KEY, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $store));
    }

    /**
     * @param      $formId
     * @param null $store
     *
     * @return bool
     */
    public function isCaptcha($formId, $store = null) {
        if ($this->scopeConfig->getValue(self::XML_PATH_CUSTOMER_CREATE_ACCOUNT_ENABLE_RECAPTCHA, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $store) == 0) {
            return false;
        }
        $forms = explode(',', $this->scopeConfig->getValue(self::XML_PATH_CUSTOMER_CREATE_ACCOUNT_RECAPTCHA_FORMS_KEY, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $store));

        return in_array($formId, $forms);
    }

    /**
     * @param      $response
     * @param null $store
     *
     * @return bool
     */
    public function verifyResponse($response, $store = null) {
        $data = array(
            'secret'   => $this->getRecaptchaSecretKey($store),
            'response' => $response
        );

        try {
            $verify = curl_init();
            curl_setopt($verify, CURLOPT_URL, self::URL_RECAPTCHA_API_SITEVERIFY);
            curl_setopt($verify, CURLOPT_POST, true);
            curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($verify);
            $response = json_decode($response);
            if ($response->success) {
                return true;
            }
        } catch (\Exception $e) {
            $this->_logger->log(LogLevel::ERROR, $e);
        }

        return false;
    }
}
