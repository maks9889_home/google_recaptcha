<?php
namespace Northern\GoogleRecaptcha\Block;

class Captcha extends \Magento\Framework\View\Element\Template {
    protected $helper;

    /**
     * Captcha constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Northern\GoogleRecaptcha\Helper\Data            $helper
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Northern\GoogleRecaptcha\Helper\Data $helper,
        array $data = []
    ) {
        // default Template. It can be rewritten in layout if needed
        $this->setTemplate('Northern_GoogleRecaptcha::recaptcha.phtml');
        $this->helper = $helper;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }

    public function getRecaptchaSiteKey() {
        return $this->helper->getRecaptchaSiteKey();
    }

    /**
     * Renders captcha HTML (if required)
     *
     * @return string
     */
    protected function _toHtml() {
        if ($this->helper->isCaptcha($this->getFormId())) {

            return parent::_toHtml();
        }

        return '';
    }
}
